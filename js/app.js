let firstNumber;
let secondNumber;
let n;
while (!firstNumber || Math.floor(firstNumber) !== firstNumber) {
  firstNumber = +prompt("Enter first natural number");
}
while (!secondNumber || Math.floor(secondNumber) !== secondNumber) {
  secondNumber = +prompt("Enter second natural number");
}
while (!n || Math.floor(n) !== n) {
  n = +prompt("Enter n number");
}
function fib() {
  let a = firstNumber;
  let b = secondNumber;
  let c;
  for (let i = 1; i <= n; i++) {
    c = a + b;
    a = b;
    b = c;
  }
  return c;
}
alert(`Your Fibonacci number is ${fib(firstNumber, secondNumber, n)}.`);